import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MockOrdersProvider } from '../mock-orders/mock-orders';

/*
  Generated class for the OrdersRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrdersRestProvider {
  orders: any[];
  properties: Array<any> 
  constructor(public http: HttpClient, public mockOders: MockOrdersProvider) {
   
   this.findAll();
  }

  findAll() {
    return this.orders;
}

findById(id) {
    return Promise.resolve(this.orders[id - 1]);
}

getItem(id) {
  for (var i = 0; i < this.orders.length; i++) {
    if (this.orders[i].id === parseInt(id)) {
      return this.orders[i];
    }
  }
  return null;
}
findByName(searchKey: string) {
  let key: string = searchKey.toUpperCase();
  return Promise.resolve(this.orders.filter((order: any) =>
    (order.Tracer ).toUpperCase().indexOf(key) > -1));
}




}
