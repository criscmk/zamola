import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the TasksProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TasksProvider {

  duties: any[];
  properties: Array<any> 
  constructor(public http: HttpClient) {
   
   this.findAll();
  }

  findAll() {
    return this.duties;
}

findById(id) {
    return Promise.resolve(this.duties[id - 1]);
}

getItem(id) {
  for (var i = 0; i < this.duties.length; i++) {
    if (this.duties[i].id === parseInt(id)) {
      return this.duties[i];
    }
  }
  return null;
}
findByName(searchKey: string) {
  let key: string = searchKey.toUpperCase();
  return Promise.resolve(this.duties.filter((order: any) =>
    (order.Tracer ).toUpperCase().indexOf(key) > -1));
}




}
