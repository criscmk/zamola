import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  serverUrl: string = 'https://zamola.ca/api/'
  httpOptions: any = {};
  loginAccessToken: any;
  accessToken: String;

  distributionCenter: any = { lat: '', lng: '' };
  lat1: number;
  lng1: number;
  cost_per_km: any;

  processing: boolean;
	name: any;
	email: any;
  userDetails: any;
	role: any;
  step1Data: any;
  step2Data: any;
  step3Data: any;
  count: number;
	state: any;
  allOrders: any;
  allcount: number;
  pendingCount: any;
  completeCount: any;
  processingCount: any;
  approvedCount: any;
  intransitCount: any;
  unpaidCount: any;
  alltasks: any;
  Lname: any;
  lname: any;
  phone: any;
  address: any;
  
  // created_at: "2019-04-02 08:13:36"
  // duty_status: "0"
  // email: "labankimi@gmail.com"
  // email_verified_at: null
  // id: 16
  // idno: null
  // name: "Laban Cheruiyot"
  // phone: "+254719327570"
  // role: "client"
  // status: "0"
  // updated_at: "2019-04-02 08:13:36"

  constructor(public http: HttpClient) {

    // create the set of http headers accepted by the server
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Accept": 'application/json'
      })
    };

  }

  // pass in data and url (using the server url)
  templateLoader(url, data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.serverUrl + url, data, this.httpOptions)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    })
  }

  mainPost(url, data) {
    const reqOpts = {
      headers: {
        'Authorization': 'Bearer ' + this.loginAccessToken,
        'Content-Type': 'application/json',
      },
    };
    console.log("http headers", reqOpts);
    return new Promise((resolve, reject) => {
      this.http.post(this.serverUrl + url, data, reqOpts)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    })
  }

  mainGet(url) {

    const reqOpts = {
      headers: {
        'Authorization': 'Bearer ' + this.loginAccessToken,
        'Content-Type': 'application/json',
      },
    };

    console.log("http headers2", reqOpts);
    return this.http.get(this.serverUrl + url, reqOpts)
  }


  Get2(url) {
    return this.http.get(this.serverUrl + url)
  }

}