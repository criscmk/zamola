import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import QRCode from 'qrcode';

/**
 * Generated class for the OrderdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orderdetails',
  templateUrl: 'orderdetails.html',
})
export class OrderdetailsPage {
  details: any;
  trace: any;
  Shipment_Category: any;
  quantity: any;
  length: any;
  weight: any;
  width: any;
  payment: any;
  height: any;
  tracer: any;
  code = 'some sample string';
  generated = '';
  displayQrCode() {
    return this.generated !== '';
  }


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderdetailsPage');
    this.details = this.navParams.get('details');
    console.log('details',this.details);
    this.tracer = this.details.Tracer
    this.Shipment_Category= this.details.Shipment_Category;
    this.quantity = this.details.Quantity,
    this.weight = this.details.Weight,
    this.length = this.details.Length,
    this.width = this.details.Width,
    this.height = this.details.Height,
    this.payment = this.details.Payment
    this.process();
    
  }
  process() {
    this.code = this.tracer;
    const qrcode = QRCode;
    const self = this;
    qrcode.toDataURL(self.code, { errorCorrectionLevel: 'H' }, function (err, url) {
      self.generated = url;
    })
  }

}
