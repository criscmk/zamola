import { Component } from "@angular/core";
import { IonicPage, NavController, ModalController, NavParams, AlertController, LoadingController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { RestProvider } from "../../providers/rest/rest";
@IonicPage({
	name: 'page-settings',
	segment: 'settings'
})

@Component({
	selector: 'page-settings',
	templateUrl: 'settings.html'
})
export class SettingsPage {
	category: any;
	quantity: any;
	length: any;
	width: any;
	height: any;
	weight: any;
	payment: any;
	sendermobile: any;
	sendername: any;
	senderaddress: any;
	senderemail: any;
	senderlocation: any;
	receivermobile: any;
	receivername: any;
	receiveraddress: any;
	receiveremail: any;
	receiverlocation: any;


	constructor(public navCtrl :NavController,
		public modalCtrl: ModalController,
		public navParams: NavParams,
		public restProvider: RestProvider,
		public loadingCtrl: LoadingController,
		public resProvider: RestProvider,
		public alertCtrl :AlertController, ) {
		this.category = this.resProvider.step1Data.category,
			this.quantity = this.resProvider.step1Data.quantity,
			this.weight = this.resProvider.step1Data.weight,
			this.length = this.resProvider.step1Data.length,
			this.width = this.resProvider.step1Data.width,
			this.height = this.resProvider.step1Data.height,
			this.payment = this.resProvider.step1Data.payment,
			this.sendername = this.resProvider.step2Data.name,
			this.sendermobile = this.resProvider.step2Data.phone,
			this.senderaddress = this.resProvider.step2Data.address,
			this.senderemail = this.resProvider.step2Data.email,
			this.senderlocation = this.resProvider.step2Data.location,
			this.receivername = this.resProvider.step3Data.reecievername,
			this.receivermobile = this.resProvider.step3Data.receivermobile,
			this.receiveraddress = this.resProvider.step3Data.receiveraddress,
			this.receiveremail = this.resProvider.step3Data.receiveremail,
			this.receiverlocation = this.resProvider.step3Data.receiverlocation
	}


}
