import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
// import { MessageService } from '../../providers/message-service-mock';
import { OrdersRestProvider } from '../../providers/orders-rest/orders-rest';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  state: any = 1;
  searchKey: string = "";
  messages: Array<any> = [];
  myOrders: any = [];
  AllOrders: any[];
  orders: any;
  orderstate: number;
  allorders: any;
  pending: any = [];
  processing: any = [];
  complete: any = [];

  constructor(public navCtrl: NavController,
    public OrdersProvider: OrdersRestProvider,
    private menuCtrl: MenuController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public restProvider: RestProvider,
    public loadingCtrl: LoadingController,
    //public service: MessageService
  ) {
    this.menuCtrl.enable(true);
    this.allOrders();
  }

  itemTapped(order) {
    // console.log('itemTapped: ', message);
    this.navCtrl.push('OrderdetailsPage', { 'details': order });
  }

  // deleteItem(message) {
  //   this.service.delMessage(message);
  // }

  // getMessages() {
  //   this.messages = this.service.getMessages();
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.state = this.navParams.get('state');
    console.log('State', this.state);

  }
  create() {
    this.navCtrl.setRoot('NewOrderStep1Page');
  }
  onInput(event) {

    this.OrdersProvider.findByName(this.searchKey)
      .then(data => {
        this.myOrders = data;
      })
      .catch(error => alert(JSON.stringify(error)));
  }
  allOrders() {
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })

    //Change network status

    loading.present()
    this.restProvider.mainGet('order').subscribe((result) => {
      loading.dismiss();
      console.log(result);
      this.orders = result;
      this.allorders = this.orders.data;
      console.log('all orders', this.allorders);
      this.OrdersProvider.orders = this.allorders;
      this.myOrders = this.OrdersProvider.orders;
      this.AllOrders = this.OrdersProvider.orders;
      console.log("Orders zangu", this.myOrders.length);
      this.restProvider.allcount = this.myOrders.length;
      this.restProvider.allOrders = this.allorders

      console.log("Orders from the provider", this.OrdersProvider.orders);
      if (this.allOrders == null) {
        console.log('hakuna matata')
        this.orderstate == 0;
      }
      {
        this.getCounts();
        this.orderstate == 1;
      }
    }, error => {
      loading.dismiss()
      console.log("error: ", error)
      this.alertCtrl.create({
        title: ' Error',
        message: 'Something went wrong',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      }).present()
    });

  }
  getCounts() {
    this.AllOrders.forEach(element => {
      let count = 1
      if (element.Status == "pending") {
        this.pending.push(element);
      }

    });
    this.restProvider.pendingCount = this.pending.length;


    this.AllOrders.forEach(element => {

      if (element.Status == "complete") {
        this.complete.push(element);
      }

    });
    this.restProvider.completeCount = this.complete.length;
    this.AllOrders.forEach(element => {

      if (element.Status == "processing") {
        this.processing.push(element);
      }

    });
    this.restProvider.processingCount = this.processing.length;

  }



}
