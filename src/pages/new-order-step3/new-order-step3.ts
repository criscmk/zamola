import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { regexValidators } from '../validators/validators';

/**
 * Generated class for the NewOrderStep3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-order-step3',
  templateUrl: 'new-order-step3.html',
})
export class NewOrderStep3Page {
  OrderStep3: any;
  finalData: {};
  response: any;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public resProvider: RestProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.OrderStep3 = formBuilder.group({
      reecievername: ['', Validators.compose([Validators.required])],
      receiverlocation: ['', Validators.compose([Validators.required,])],
      receiveraddress: ['', Validators.compose([Validators.required,])],
      receiveremail: ['', Validators.compose([Validators.required,])],
      receiverphone: [
        '', Validators.compose([Validators.pattern(regexValidators.telephone),
        Validators.required
        ])]

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewOrderStep3Page');

  }
  next() {

    this.resProvider.step3Data = this.OrderStep3.value;
    console.log('All details', this.resProvider.step1Data);
    console.log('All details1', this.resProvider.step2Data);

    this.finalData = {
      category: this.resProvider.step1Data.category,
      quantity: this.resProvider.step1Data.quantity,
      weight: this.resProvider.step1Data.weight,
      length: this.resProvider.step1Data.length,
      width: this.resProvider.step1Data.width,
      height: this.resProvider.step1Data.height,
      payment: this.resProvider.step1Data.payment,
      sendername: this.resProvider.step2Data.name,
      sendermobile: this.resProvider.step2Data.phone,
      senderaddress: this.resProvider.step2Data.address,
      senderemail: this.resProvider.step2Data.email,
      senderlocation: this.resProvider.step2Data.location,
      receivername: this.resProvider.step3Data.reecievername,
      receivermobile: this.resProvider.step3Data.receiverphone,
      receiveraddress: this.resProvider.step3Data.receiveraddress,
      receiveremail: this.resProvider.step3Data.receiveremail,
      receiverlocation: this.resProvider.step3Data.receiverlocation,

    }

    console.log('All details2', this.finalData);
    let loading = this.loadingCtrl.create({
      content: 'Placing your Order...',
      spinner: 'bubbles'
    })
    loading.present()
    this.resProvider.mainPost('order', this.finalData).then((result) => {
      console.log(result)
      this.response = result
      if (this.response.status == 200) {
        loading.dismiss()
        this.alertCtrl.create({
          title: 'Success',
          message: 'Your order has been placed successfully',
          mode: 'ios',
          buttons: [{
            text: 'dismiss',
            handler: () => {


            }
          }]
        }).present()
        this.navCtrl.setRoot('page-settings');
      }
      else {
        loading.dismiss()
        this.alertCtrl.create({
          title: 'Warning',
          message: this.response.message,
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {

            }
          }]
        }).present()
      }
    }, error => {
      console.log(error);
      loading.dismiss();

    })





  }
  sendfinaldata() {

  }
}
