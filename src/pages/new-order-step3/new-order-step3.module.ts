import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewOrderStep3Page } from './new-order-step3';

@NgModule({
  declarations: [
    NewOrderStep3Page,
  ],
  imports: [
    IonicPageModule.forChild(NewOrderStep3Page),
  ],
})
export class NewOrderStep3PageModule {}
