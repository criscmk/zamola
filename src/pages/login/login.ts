import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  inputType: string = 'password'
  loginForm: FormGroup
  response: any;
  user: any;
  profile: any;

  constructor(
    public navParams: NavParams, private formBuilder: FormBuilder,
    public forgotCtrl: AlertController,
    private toastCtrl: ToastController,
    private restProvider: RestProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController,
    private navCtrl: NavController, private storage: Storage) {

    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  // switch the input type of password field to reveal or hide password accordingly
  switchInputType(type) {
    if (type === 'password') {
      this.inputType = 'password'
    } else if (type === 'text') {
      this.inputType = 'text'
    }
  }

  // send user credentials to the server for validation
  signIn() {
    console.log('login details', this.loginForm.value);

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'crescent'
    })
    let signinData = {
      'email': this.loginForm.controls.username.value,
      "password": this.loginForm.controls.password.value
    }


    loading.present()

    this.restProvider.templateLoader('login', signinData).then((result) => {
      // loading.dismiss()
      console.log(result)

      this.response = result

      this.restProvider.loginAccessToken = this.response.accessToken

      if (this.restProvider.loginAccessToken) {
        loading.dismiss();
        this.storage.set('STORAGE_KEY_TOKEN', this.response.accessToken);
        
        this.getProfile();
      }
      else {
        loading.dismiss()
        this.alertCtrl.create({
          title: 'Sign in Error',
          message: 'Something went wrong. Please try again.',
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {

            }
          }]
        }).present()

      }
    }, error => {
      loading.dismiss()
      console.log("error: ", error)
      this.alertCtrl.create({
        title: 'Sign in Error',
        message: 'Invalid username/ password',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      }).present()
    });

  }

  // navigate the user to the registration page
  newUser() {
    this.navCtrl.push('UserRegistrationPage')
  }

  // get user details
  getProfile() {

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })

    loading.present()
    this.restProvider.mainGet('user').
      subscribe(res => {
        loading.dismiss();
        this.profile = res;
        console.log('company profile', this.profile);
      
        this.storage.set('profile', this.profile.data);
        this.restProvider.email = this.profile.email;
        this.restProvider.role = this.profile.role_id;
        this.restProvider.userDetails = this.profile
        this.restProvider.name = this.profile.name;
        this.storage.set('profile', this.profile);
        if (this.restProvider.role==2)
		{
			this.navCtrl.setRoot('HomePage');
			console.log('cmk');
		}
		else if(this.restProvider.role==3)
		{
			
			this.navCtrl.setRoot('MydutiesPage')
			console.log('cris');
		}
      }, error => {
        loading.dismiss();
        console.log("Error", error)
      });

  }



  forgotpass() {
    this.navCtrl.setRoot('ForgotpasswordPage');
  }
}
