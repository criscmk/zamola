import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecificordersPage } from './specificorders';

@NgModule({
  declarations: [
    SpecificordersPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecificordersPage),
  ],
})
export class SpecificordersPageModule {}
