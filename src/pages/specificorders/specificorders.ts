import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { OrdersRestProvider } from '../../providers/orders-rest/orders-rest';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the SpecificordersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-specificorders',
  templateUrl: 'specificorders.html',
})
export class SpecificordersPage {
  orders: any;
  allorders: any;
  myOrders: any;
  AllOrders: any=null;
  orderstate: number;
  state: any;
  searchKey: string = "";
  specificOrder: any=[];
  findpending: any;
  title: string;

  constructor(public navCtrl: NavController,
    public OrdersProvider: OrdersRestProvider,
    private menuCtrl: MenuController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public restProvider: RestProvider,
    public loadingCtrl: LoadingController,
    //public service: MessageService
  ) {

    this.menuCtrl.enable(true);
   
  }

  itemTapped(message) {
    // console.log('itemTapped: ', message);
    this.navCtrl.push('page-message-detail', {
      'id': message.id
    });
  }

  // deleteItem(message) {
  //   this.service.delMessage(message);
  // }

  // getMessages() {
  //   this.messages = this.service.getMessages();
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.state = this.navParams.get('state');
    console.log('State', this.state);


    if(this.state==2)
    {
      this.pendingOrders();
    }
    else if(this.state==3)
    {
      this.processingOrders();
    }
    else if(this.state == 4)
    {
      this.completeOrders();
    }
    

  }
  create() {
    this.navCtrl.setRoot('NewOrderStep1Page');
  }
  onInput(event) {

    this.OrdersProvider.findByName(this.searchKey)
      .then(data => {
        this.myOrders = data;
      })
      .catch(error => alert(JSON.stringify(error)));
  }
  pendingOrders() {
 this.findpending = this.restProvider.allOrders;
 this.findpending.forEach(element => {
   if(element.Status=="pending")
   this.specificOrder.push(element);
   
 });
 console.log('pending orders',this.specificOrder);
 this.OrdersProvider.orders = this.specificOrder;
 this.myOrders = this.OrdersProvider.orders;
 this.orderstate = this.OrdersProvider.orders.length;
 this.title = "Pending orders";
  }

  processingOrders() {
    this.findpending = this.restProvider.allOrders;
    this.findpending.forEach(element => {
      if(element.Status=="processing")
      this.specificOrder.push(element);
      
    });
    console.log('pending orders',this.specificOrder);
    this.OrdersProvider.orders = this.specificOrder;
    this.myOrders = this.OrdersProvider.orders;
    this.orderstate = this.OrdersProvider.orders.length
    this.title = "processing orders";
     }
     completeOrders() {
      this.findpending = this.restProvider.allOrders;
      this.findpending.forEach(element => {
        if(element.Status=="complete")
        this.specificOrder.push(element);
        
      });
      console.log('complete',this.specificOrder);
      this.OrdersProvider.orders = this.specificOrder;
      this.myOrders = this.OrdersProvider.orders;
      this.orderstate = this.OrdersProvider.orders.length;
      this.title = "complete orders";
       }
   

}

