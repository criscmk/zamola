import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, AlertController, ToastController, MenuController, LoadingController } from 'ionic-angular';
import { regexValidators } from '../validators/validators';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
@IonicPage({
  name: 'page-auth',
  segment: 'auth',
  priority: 'high'
})
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html'
})
export class AuthPage implements OnInit {
  public onLoginForm: FormGroup;
  public onRegisterForm: FormGroup;
  auth: string = "login";
  inputType: string = 'password'
  RegDetails: any;
  response: any;
  errorResponse: any = [];
  LoginDetails: { 'email': any; "password": any; };
  profile: any;
  user: any;
  constructor(
    private _fb: FormBuilder,
    public nav: NavController,
    public forgotCtrl: AlertController,
    public menu: MenuController,
    public toastCtrl: ToastController,
    private restProvider: RestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage,
  ) {
    this.menu.swipeEnable(false);
    this.menu.enable(false);
  }
  ngOnInit() {
    this.onLoginForm = this._fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([
        Validators.required
      ])]
    });

    this.onRegisterForm = this._fb.group({

      email: ['', Validators.compose([Validators.required, Validators.email])],
      name: ['', Validators.compose([Validators.required,])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      //id_Number: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(8)])],
      passwordConfirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])],

    });
  }
  switchInputType(type) {
    if (type === 'password') {
      this.inputType = 'password'
    } else if (type === 'text') {
      this.inputType = 'text'
    }
    console.log('make it', this.inputType);
  }
  // go to register page
  // register() {
  //   this.nav.setRoot(RegisterPage);
  // }

  // login and go to home page
  login() {
    // this.nav.setRoot('HomePage');
    this.LoginDetails =
      {
        'email': this.onLoginForm.controls.email.value,
        "password": this.onLoginForm.controls.password.value

      }
    console.log('login details', this.LoginDetails);

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    loading.present()
    this.restProvider.templateLoader('login', this.LoginDetails).then((result) => {
      // loading.dismiss()
      console.log(result);
      console.log("result");
      this.response = result
      this.restProvider.loginAccessToken = this.response.accessToken
      if (this.restProvider.loginAccessToken) {
        loading.dismiss();
        this.storage.set('STORAGE_KEY_TOKEN', this.response.accessToken);
        console.log("User Details",);
        let userDetails= this.response.user;
        this.storeProfile(userDetails)
        this.nav.setRoot('HomePage');
      }
      else {
        loading.dismiss()
        this.alertCtrl.create({
          title: 'Sign in Error',
          message: 'Something went wrong. Please try again.',
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {
            }
          }]
        }).present()

      }
    }, error => {
      loading.dismiss()
      console.log("error: ", error)
      this.alertCtrl.create({
        title: 'Sign in Error',
        message: 'invalid username/password',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      }).present()
    });
  }
  register() {
    this.RegDetails =
      {

        'email': this.onRegisterForm.controls.email.value,
        'name': this.onRegisterForm.controls.name.value,
        'password_confirmation': this.onRegisterForm.controls.passwordConfirmation.value,
        'password': this.onRegisterForm.controls.password.value
      }
    console.log('Registration details', this.RegDetails);
    console.log('reg data', this.RegDetails);
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    loading.present()
    this.restProvider.templateLoader('register', this.RegDetails).then((result) => {
      loading.dismiss();
      console.log(result);
      this.response = result;
      this.restProvider.loginAccessToken = this.response.accessToken;
      if (this.restProvider.loginAccessToken) {
        this.storage.set('STORAGE_KEY_TOKEN', this.response.accessToken);
        let userDetails= this.response.user;
        this.storeProfile(userDetails)
        this.nav.setRoot('HomePage');
      }
      else {
        this.validate();
      }

    }, error => {
    })
  }
  validate() {
    if (this.response.email) {
      this.errorResponse.push(this.response.email[0]);
      this.alertCtrl.create({
        title: 'Sign Up Error',
        message: this.response.email,
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {
            this.errorResponse = [];
          }
        }]
      }).present()

    } if (this.response.phone_number) {

      this.errorResponse.push(this.response.phone_number[0])
      this.alertCtrl.create({
        title: 'Sign Up Error',
        message: this.response.phone,
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {
            this.errorResponse = [];
          }
        }]
      }).present()
    } if (this.response.id_no) {
      this.alertCtrl.create({
        title: 'Sign Up Error',
        message: this.response.id_no,
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {
            this.errorResponse = [];
          }
        }]
      }).present()
    } else {
      return null
    }
  }
  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }
  sociallogin(data) {
    console.log("user details  : ", data);
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 3000);
    this.restProvider.templateLoader('social-login', data).then((result) => {
      this.response = result;

      console.log("resposnse from server", this.response);
      this.restProvider.loginAccessToken = this.response.accessToken;
      loading.dismiss().then(() => {

        console.log("loginAccesToken", this.response.accessToken);
        if (this.restProvider.loginAccessToken) {
          this.storage.set('STORAGE_KEY_TOKEN', this.response.accessToken);

        }
        else {
          this.alertCtrl.create({
            title: 'Sign Up Error',
            message: 'Something went wrong. Please try again.',
            mode: 'ios',
            buttons: [{
              text: 'Dismiss',
              handler: () => {
                this.nav.setRoot('AuthPage')
              }
            }]
          }).present()
        }
      });


    }, error => {
      loading.dismiss();
      console.log(error)
    });
  }
  storeProfile(details)
  {
    this.restProvider.userDetails = details
    this.restProvider.name = details.name;
    this.restProvider.email = details.email;
    this.restProvider.role = details.role_id;
    this.storage.set('profile',details);
  }

}
