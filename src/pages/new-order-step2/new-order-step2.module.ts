import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewOrderStep2Page } from './new-order-step2';

@NgModule({
  declarations: [
    NewOrderStep2Page,
  ],
  imports: [
    IonicPageModule.forChild(NewOrderStep2Page),
  ],
})
export class NewOrderStep2PageModule {}
