import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { regexValidators } from '../validators/validators';

/**
 * Generated class for the NewOrderStep2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-order-step2',
  templateUrl: 'new-order-step2.html',
})
export class NewOrderStep2Page {

  OrderStep2: any;

  constructor(public navCtrl: NavController,
    public resProvider: RestProvider,
    public formBuilder: FormBuilder, public navParams: NavParams) {
    this.OrderStep2 = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      location: ['', Validators.compose([Validators.required,])],
      address: ['', Validators.compose([Validators.required,])],
      email: ['', Validators.compose([Validators.required,])],
      phone: [
        '', Validators.compose([Validators.pattern(regexValidators.telephone),
        Validators.required
        ])]


    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewOrderStep2Page');
  }
  next() {
    this.navCtrl.setRoot('NewOrderStep3Page');
    this.resProvider.step2Data = this.OrderStep2.value;
  }
}
