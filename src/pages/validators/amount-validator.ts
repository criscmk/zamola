import { FormControl } from '@angular/forms';

export class AmountValidator {

    static isValid(control: FormControl): any {
        if (isNaN(control.value)) {
            return {
                "Enter a valid number": true
            };
        }

        if (control.value < 1000) {
            return {
                "Amount must be greater than 1000": true
            };
        }

        return null;
    }

}