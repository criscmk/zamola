import { FormControl } from '@angular/forms';

export class PeriodValidator {

    static isValid(control: FormControl): any {
        if (isNaN(control.value)) {
            return {
                "Enter a valid period": true
            };
        }

        if (control.value < 1) {
            return {
                "Period cannot be less than 1 month": true
            };
        }

        if (control.value > 12) {
            return {
                "Period cannot be greater than 12 months": true
            };
        }

        return null;
    }

}