import { FormControl } from '@angular/forms';

export class PhoneNumberValidator {

    static isValid(control: FormControl): any {


        if (control.value.startsWith("+254") && control.value.replace(" ", "").length != 13 && control.value.replace("-", "").length != 13) {

            return {
                "invalid": true
            };

        } else if (control.value.startsWith("254") && control.value.replace(" ", "").length != 12 && control.value.replace("-", "").length != 12) {

            return {
                "invalid": true
            };

        } else if (control.value.startsWith("7") && control.value.replace(" ", "").length != 9 && control.value.replace("-", "").length != 9) {
            return {
                "invalid": true
            };

        } else if (control.value.startsWith("0") && control.value.replace(" ", "").length != 10 && control.value.replace("-", "").length != 10) {
            return {
                "invalid": true
            };

        }

        return null;

    }
}