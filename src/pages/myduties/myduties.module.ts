import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MydutiesPage } from './myduties';

@NgModule({
  declarations: [
    MydutiesPage,
  ],
  imports: [
    IonicPageModule.forChild(MydutiesPage),
  ],
})
export class MydutiesPageModule {}
