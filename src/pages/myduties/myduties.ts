import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { TasksProvider } from '../../providers/tasks/tasks';

/**
 * Generated class for the MydutiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myduties',
  templateUrl: 'myduties.html',
})
export class MydutiesPage {
  tasks: any;
  alltasks: any;
  tasktate: number;
  searchKey: string = "";
  approved: any = [];
  intransit: any = [];
  unpaid: any = [];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public dutiesProvider: TasksProvider,
    public alertCtrl: AlertController
  ) {
    this.myDuties();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Duties Page');
  }
  itemTapped(order) {
    // console.log('itemTapped: ', message);
    this.navCtrl.push('TasksDetailsPage', { 'details': order });
  }

  onInput(event) {

    this.dutiesProvider.findByName(this.searchKey)
      .then(data => {
        this.alltasks = data;
      })
      .catch(error => alert(JSON.stringify(error)));
  }

  myDuties() {
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    //Change network status
    loading.present()
    this.restProvider.mainGet('duty').subscribe((result) => {
      loading.dismiss();
      console.log(result);
      this.tasks = result;
      this.tasks = this.tasks.data;
      this.restProvider.alltasks = this.tasks;
      this.getCounts();

      this.tasks.forEach(element => {

        if (element.Status == "approved") {
          this.approved.push(element);
        }
      });
      this.alltasks = this.approved;

      this.dutiesProvider.duties = this.approved;

      console.log('all orders', this.alltasks);

      if (this.alltasks.length === 0) {
        console.log('hakuna matata')
        this.tasktate = 0;
      }
      else {

        this.tasktate == 1;
      }
    }, error => {
      loading.dismiss()
      console.log("error: ", error)
      this.alertCtrl.create({
        title: ' Error',
        message: 'Something went wrong',
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {

          }
        }]
      }).present()
    });

  }
  getCounts() {
    this.tasks.forEach(element => {
      let count = 1
      if (element.Status == "approved") {
        this.approved.push(element);
      }
    });
    this.restProvider.approvedCount = this.approved.length;
    this.tasks.forEach(element => {
      if (element.Status == "intransit") {
        this.intransit.push(element);
      }
    });
    this.restProvider.intransitCount = this.intransit.length;
    console.log('orders on Transit', this.restProvider.intransitCount);
    this.tasks.forEach(element => {
      if (element.Status == "unpaid") {
        this.unpaid.push(element);
      }

    });
    console.log('Upaid orders', this.unpaid);
    this.restProvider.unpaidCount = this.unpaid.length;
  }
  duties() {

  }
}

