import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the NewOrderStep1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-order-step1',
  templateUrl: 'new-order-step1.html',
})
export class NewOrderStep1Page {
  OrderStep1: any;
  response: any;
  categories: any;
  payments: any;

  constructor(public navCtrl: NavController,
    public loadingCtrl:LoadingController,
    public formBuilder: FormBuilder, public navParams: NavParams,
    public resProvider: RestProvider) {
    this.OrderStep1 = formBuilder.group({
      category: ['', Validators.compose([Validators.required])],
      quantity: ['', Validators.compose([Validators.required,])],
      height: ['', Validators.compose([Validators.required,])],
      width: ['', Validators.compose([Validators.required,])],
      weight: ['', Validators.compose([Validators.required,])],
      length: ['', Validators.compose([Validators.required,])],
      payment: ['', Validators.compose([Validators.required,])],
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewOrderStep1Page');
  }
  ionViewWillLoad() {
    this.getPayment();
    this.getCategories();

  }
  next() {
    this.navCtrl.setRoot('NewOrderStep2Page');
    console.log('step 1 details', this.OrderStep1.value);
    this.resProvider.step1Data = this.OrderStep1.value
  }
  //get categories
  getCategories() {

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })

    loading.present()
    this.resProvider.mainGet('shipment_categories').
      subscribe(res => {
        loading.dismiss();
        this.response = res;
        this.categories = this.response.data;
        console.log('categories',this.categories);
       
       
      }, error => {
        loading.dismiss();
        console.log("Error", error)
      });

  }
    //get payment
    getPayment() {

      let loading = this.loadingCtrl.create({
        content: 'Processing...',
        spinner: 'bubbles'
      })
  
      loading.present()
      this.resProvider.mainGet('payment_methods').
        subscribe(res => {
          loading.dismiss();
          this.response = res;
          this.payments = this.response.data;
          console.log('payments',this.payments);
         
         
        }, error => {
          loading.dismiss();
          console.log("Error", error)
        });
  
    }
  
    ridetype() {
     console.log('good');
    }

}
