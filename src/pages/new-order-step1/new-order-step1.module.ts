import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewOrderStep1Page } from './new-order-step1';

@NgModule({
  declarations: [
    NewOrderStep1Page,
  ],
  imports: [
    IonicPageModule.forChild(NewOrderStep1Page),
  ],
})
export class NewOrderStep1PageModule {}
