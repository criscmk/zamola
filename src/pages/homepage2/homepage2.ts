import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { TasksProvider } from '../../providers/tasks/tasks';

/**
 * Generated class for the Homepage2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homepage2',
  templateUrl: 'homepage2.html',
})
export class Homepage2Page {
  tasks: any;
  alltasks: any;
  tasktate: number;
  searchKey: string = "";
  approved: any = [];
  intransit: any = [];
  unpaid: any = [];
  state: any;
  taskstate: any;
  title: string;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public dutiesProvider: TasksProvider,
    public alertCtrl: AlertController
  ) {
    this.alltasks = this.restProvider.alltasks;
    this.state = this.navParams.get('state');
    console.log('current state', this.state);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Homepage2Page');
    this.findduties();

  }
  itemTapped(order) {
    // console.log('itemTapped: ', message);
    this.navCtrl.push('TasksDetailsPage', { 'details': order });
  }

  onInput(event) {

    this.dutiesProvider.findByName(this.searchKey)
      .then(data => {
        this.alltasks = data;
      })
      .catch(error => alert(JSON.stringify(error)));
  }

  // allOrders() {
  //   let loading = this.loadingCtrl.create({
  //     content: 'Processing...',
  //     spinner: 'bubbles'
  //   })
  //   //Change network status
  //   loading.present()
  //   this.restProvider.mainGet('duty').subscribe((result) => {
  //     loading.dismiss();
  //     console.log(result);
  //     this.tasks = result;
  //     this.alltasks = this.tasks.data;
  //     this.dutiesProvider.duties = this.alltasks;
  //     console.log('all orders', this.alltasks);

  //     if (this.alltasks.length === 0) {
  //       console.log('hakuna matata')   
  //       this.tasktate == 0;
  //     }
  //     else {
  //       this.getCounts();
  //       this.tasktate == 1;
  //     }
  //   }, error => {
  //     loading.dismiss()
  //     console.log("error: ", error)
  //     this.alertCtrl.create({
  //       title: ' Error',
  //       message: 'Something went wrong',
  //       mode: 'ios',
  //       buttons: [{
  //         text: 'Dismiss',
  //         handler: () => {

  //         }
  //       }]
  //     }).present()
  //   });

  // }
  getCounts() {
    this.alltasks.forEach(element => {
      let count = 1
      if (element.Status == "approved") {
        this.approved.push(element);
      }
    });
    this.restProvider.approvedCount = this.approved.length;
    this.alltasks.forEach(element => {

      if (element.Status == "intransit") {
        this.intransit.push(element);
      }
    });
    this.restProvider.intransitCount = this.intransit.length;
    console.log('orders on Transit', this.restProvider.intransitCount);
    this.alltasks.forEach(element => {
      if (element.Status == "unpaid") {
        this.unpaid.push(element);
      }

    });
    console.log('Upaid orders', this.unpaid);
    this.restProvider.unpaidCount = this.unpaid.length;
  }
  findduties() {
if(this.state==2)
  {
    this.alltasks= this.intransit;
    this.taskstate = this.alltasks.length
    this.title = "Transit tasks";
    console.log('task state',this.taskstate);
  }
  if(this.state==3)
  {
    this.alltasks= this.unpaid;
    this.taskstate = this.alltasks.length;
    this.title = "Complete tasks";
  }
  if(this.state==4)
  {
    this.alltasks= this.restProvider.alltasks;
    this.taskstate = this.alltasks.length;
    this.title = "Transit Tasks";
  }

}
}
