import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Homepage2Page } from './homepage2';

@NgModule({
  declarations: [
    Homepage2Page,
  ],
  imports: [
    IonicPageModule.forChild(Homepage2Page),
  ],
})
export class Homepage2PageModule {}
