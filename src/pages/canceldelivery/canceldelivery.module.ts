import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanceldeliveryPage } from './canceldelivery';

@NgModule({
  declarations: [
    CanceldeliveryPage,
  ],
  imports: [
    IonicPageModule.forChild(CanceldeliveryPage),
  ],
})
export class CanceldeliveryPageModule {}
