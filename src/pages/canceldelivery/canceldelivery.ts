import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CanceldeliveryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-canceldelivery',
  templateUrl: 'canceldelivery.html',
})
export class CanceldeliveryPage {
  message: any;
  canceldetails = {
    
    "message":this.message
  }
  details: any;
 


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CanceldeliveryPage');
    this.details = this.navParams.get('details')
  }
  cancel()
  {
    this.navCtrl.setRoot('TasksDetailsPage',{'details':this.details});
  }

}
