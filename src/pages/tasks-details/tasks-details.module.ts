import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TasksDetailsPage } from './tasks-details';

@NgModule({
  declarations: [
    TasksDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TasksDetailsPage),
  ],
})
export class TasksDetailsPageModule {}
