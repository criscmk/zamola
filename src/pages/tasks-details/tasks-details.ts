import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the TasksDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tasks-details',
  templateUrl: 'tasks-details.html',
})
export class TasksDetailsPage {
  tracer: any;
  package: any;
  details: any;
  status: any;
  response: any;
  pickup: any;
  destination: any;

  constructor(public navCtrl: NavController, 
    public loadingCtrl:LoadingController,
    public alertCtrl: AlertController,
    public restProvider:RestProvider,
     public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TasksDetailsPage');
    this.details = this.navParams.get('details');
    console.log('details',this.details);
    this.tracer = this.details.Tracer
    this.package= this.details.Package;
    this.status = this.details.Status;
    this.pickup = this.details.sender_location
    this.destination = this.details.receiver_location
        
  }
  movestatus()
  {
   
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    loading.present()
    this.restProvider.mainGet('move_status/' + this.details.Shipment_id, ).subscribe((result) => {
      console.log(result)
      this.response = result
      if (this.response.status == 200) {
        loading.dismiss()
        this.alertCtrl.create({
          title: 'Success',
          message: 'You Have Accepted to ship the Order',
          mode: 'ios',
          buttons: [{
            text: 'dismiss',
            handler: () => {


            }
          }]
        }).present()
        
      }
      else {
        loading.dismiss()
        this.alertCtrl.create({
          title: 'Warning',
          message: 'An error Occured',
          mode: 'ios',
          buttons: [{
            text: 'Dismiss',
            handler: () => {
              this.navCtrl.setRoot('Homepage2Page');

            }
          }]
        }).present()
      }
    }, error => {
      console.log(error);
      loading.dismiss();

    })
  }
  cancel()
  {
    this.navCtrl.setRoot('CanceldeliveryPage',{'details':this.details});
  }

}
