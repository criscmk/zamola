import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the UserRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-registration',
  templateUrl: 'user-registration.html',
})
export class UserRegistrationPage {
  userData =
    {

    }

  inputType: string = 'password'
  registrationForm: FormGroup
  response: any;
  errorResponse: any = [];
  profile: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private restProvider: RestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage, ) {
    this.registrationForm = formBuilder.group({
      
     
      email: ['', Validators.compose([Validators.required, Validators.email])],
      name: ['', Validators.compose([Validators.required,])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      //id_Number: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(8)])],
      passwordConfirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRegistrationPage');
  }

  // switch the input type of password field to reveal or hide password accordingly
  switchInputType(type) {
    if (type === 'password') {
      this.inputType = 'password'
    } else if (type === 'text') {
      this.inputType = 'text'
    }
  }

  // send user data to the server to register the new user

  signUp() {
    this.userData = {
      'email': this.registrationForm.controls.email.value,
      'name': this.registrationForm.controls.name.value,
      'password_confirmation': this.registrationForm.controls.passwordConfirmation.value,
      'password': this.registrationForm.controls.password.value
    }
   

    console.log('registration data', this.userData)
    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })
    loading.present();
    this.restProvider.templateLoader('register', this.userData).then((result) => {
      loading.dismiss()
      console.log(result)
      this.response = result
      this.restProvider.loginAccessToken = this.response.accessToken
      if (this.restProvider.loginAccessToken) {
        this.storage.set('STORAGE_KEY_TOKEN', this.response.accessToken);
    
        this.getProfile()
        console.log('registration succesfull')
      }
      else {
       this.validate();
      }

    }, error => {
      loading.dismiss()
      this.alertCtrl.create({
        title: 'Server Error',
        message: 'This operation could not be completed. Please try again later.',
        mode: 'ios',
        buttons: [{
          text: 'Cancel',
          handler: () => {

          }
        }, {
          text: 'Retry',
          handler: () => {
            this.signUp()
          }
        }]
      })
    })
  }

  // navigate the user to sign in page
  signIn() {
    this.navCtrl.setRoot('LoginPage')
  }

  // get user details
  getProfile() {

    let loading = this.loadingCtrl.create({
      content: 'Processing...',
      spinner: 'bubbles'
    })

    loading.present()
    this.restProvider.mainGet('user').
      subscribe(res => {
        loading.dismiss();
        this.profile = res;
        console.log('company profile', this.profile);
       
        this.storage.set('profile', this.profile.data);
        this.restProvider.email = this.profile.email;
        this.restProvider.role = this.profile.role_id;
        this.restProvider.userDetails = this.profile
        this.restProvider.name = this.profile.name;
        this.storage.set('profile', this.profile);
        if (this.restProvider.role==2)
		{
			this.navCtrl.setRoot('HomePage');
			console.log('cmk');
		}
		else if(this.restProvider.role==3)
		{
			
			this.navCtrl.setRoot('MydutiesPage')
			console.log('cris');
		}
      }, error => {
        loading.dismiss();
        console.log("Error", error)
      });

  }
  validate() {
    console.log('respoce',this.response);
    if (this.response.email) {
      this.errorResponse.push(this.response.email[0]);
      this.alertCtrl.create({
        title: 'Sign Up Error',
       message:"Email has already been taken",
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {
            this.errorResponse = [];
          }
        }]
      }).present()

    } if (this.response.phone_number) {

      this.errorResponse.push(this.response.phone_number[0])
      this.alertCtrl.create({
        title: 'Sign Up Error',
        message:"Phone number has already been taken",
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {
            this.errorResponse = [];
          }
        }]
      }).present()


    } if (this.response.id_no) {
      this.alertCtrl.create({
        title: 'Sign Up Error',
        message: this.response.id_no,
        mode: 'ios',
        buttons: [{
          text: 'Dismiss',
          handler: () => {
            this.errorResponse = [];
          }
        }]
      }).present()
    } else {
      return null
    }

  }

}
