import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../providers/rest/rest';

export interface MenuItem {
	title: string;
	component: any;
	icon: string;
}

@Component({
	templateUrl: 'app.html'
})
export class ionPropertyApp {
	@ViewChild(Nav) nav: Nav;

	rootPage: any = '';
	showMenu: boolean = true;

	homeItem: any;

	initialItem: any;

	messagesItem: any;

	invoicesItem: any;

	timelineItem: any;

	appMenuItems: Array<MenuItem>;

	yourPropertyMenuItems: Array<MenuItem>;

	accountMenuItems: Array<MenuItem>;

	helpMenuItems: Array<MenuItem>;
	role: any;
	allcount: number;

	constructor(public platform: Platform,
		public storage: Storage, public statusBar: StatusBar,
		public restProvider: RestProvider, public splashScreen: SplashScreen) {
		this.initializeApp();
		this.helpMenuItems = [
			{ title: 'About', component: 'page-about', icon: 'information-circle' },
			{ title: 'Support', component: 'page-support', icon: 'information-circle' }
		];

	}


	initializeApp() {
		this.platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.statusBar.styleLightContent();
			this.splashScreen.hide();
			this.testStorage();
		});
		
	}

	openPage(page, state) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page, { "state": state });
		this.restProvider.state = state;

	}

	testStorage() {
		// Or to get a key/value pair
		this.storage.get('STORAGE_KEY_TOKEN').then((val) => {
			if (val) {
				this.profile();
				this.restProvider.loginAccessToken = val;
				console.log('access', this.restProvider.loginAccessToken);
				this.profile();
			}
			else if (!val) {
				this.nav.setRoot('LoginPage');
			}
		});

	}
	profile() {
		this.storage.get('profile').then((val) => {
			if (val) {
				this.restProvider.userDetails = val;
				this.restProvider.name = val.name;
				this.restProvider.email = val.email;
				this.restProvider.role =val.role_id;
				this.role = this.restProvider.role;
				console.log('profile', val);
				console.log('role',this.role);
				if(this.role==3)
				{
					this.nav.setRoot('MydutiesPage');
				}
				else if(this.role ==2)
				{
					this.nav.setRoot('HomePage');
				}
			}
			else if (!val) {

			}
		});

	}
	logout() {
		this.nav.setRoot('LoginPage');
		console.log('twende');
		this.storage.clear();
		localStorage.clear();

	}
	account() {
		this.nav.setRoot('page-my-account');
	}


}
